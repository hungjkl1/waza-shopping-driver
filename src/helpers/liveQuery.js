import Parse from 'parse';
import store from '../store'
import { openOrderForm } from '../containers/Home/Modal/actions';
export const liveQuery = async (user) => {
  Parse.initialize('hHdDuClDuzXuRxnYmWOzNZ4qxZW7MVHq61u','GtE9bEirZcp2eH4y95qb4me668oP8TM9UhA')
  Parse.serverURL=process.env.REACT_APP_SERVER_URL
  Parse.liveQueryServerURL=process.env.REACT_APP_SERVER_LIVE_QUERY
  console.log(process.env.REACT_APP_SERVER_URL)
  console.log(process.env.REACT_APP_SERVER_LIVE_QUERY)
    let query = new Parse.Query('DriverList');
    query.include('list_driver',user)
    let subscription = await query.subscribe();
    subscription.on('open', () => {
      console.log('subscription opened');
     });
     subscription.on('create', data => {
       console.log(data)
       if(data.get('currentDriver')===user) {
         store.dispatch(openOrderForm({id:data.get('service_trip_id'),driverListId: data.id}))
       }
    });
     subscription.on('update', data => {
       console.log(data)
       if(data.get('currentDriver')===user) {
        store.dispatch(openOrderForm({id:data.get('service_trip_id'),driverListId: data.id}))
      }
    });
  }