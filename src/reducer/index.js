// Redux 
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import driverReducer from '../providers/driver/reducer';
import locationReducer from '../providers/location/reducer';
import Order from '../containers/Home/Modal/reducer';
import walletReducer from '../providers/wallet/reducer';

const reducer = combineReducers({
  form: formReducer,
  currentDriver: driverReducer,
  currentLocation: locationReducer,
  order: Order,
  driverWallet: walletReducer
});
export default reducer;