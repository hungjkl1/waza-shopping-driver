import { SET_WALLET } from './constants';

export const setWallet = (data) => ({
  type: SET_WALLET,
  data
});