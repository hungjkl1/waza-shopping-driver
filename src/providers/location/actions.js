import { SET_LOCATION } from './constants';

export const setLocation = (data) => ({
  type: SET_LOCATION,
  data
})
