import Login from './Login';
import { connect } from 'react-redux';
import { handleLogin } from '../../providers/driver/thunk';

const mapStateToProps = (state) => ({
  currentDriver: state.currentDriver
});

const mapDispatchToProps = (dispatch) => ({
 login: (data) => dispatch(handleLogin(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);