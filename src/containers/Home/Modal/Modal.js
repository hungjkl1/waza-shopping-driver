import React, { useEffect } from 'react'
import { Button, Modal } from 'react-bootstrap'
import '../style.scss';
import _ from 'lodash'
import {connect} from 'react-redux'
import { openOrderForm, closeOrderForm } from './actions';
import { getBill, acceptOrder, rejectOrder } from './thunk';

 function Detail({show,handleClose,billId,getBillInfo,bill,acceptTrip,rejectTrip,driverListId}) {
     useEffect(()=>{
        if(billId) {
            getBillInfo(billId)
        }
     },[billId,getBillInfo])
    return (
        <React.Fragment>
            <div >
                <Modal className="modal-find-driver" show={show} onHide={handleClose}>
                    <Modal.Header>
                        <Modal.Title className="text-center">New Order</Modal.Title>
                    </Modal.Header>


                    <Modal.Body >
                        {!_.isEmpty(bill) && (
                                <div>
                                    <div className="modal-info">
                                        <label className="modal-title">Passenger Name: </label>
                                        <label className="modal-display-value">{bill.user.fullName}</label>
                                    
                                        <label className="modal-title">Tổng tiền: </label>
                                        <label className="modal-display-value">{bill.total}</label>

                                        <label className="modal-title">Phí ship: </label>
                                        <label className="modal-display-value">{bill.shippingFee}</label>

                                        <label className="modal-title">Khoảng cách: </label>
                                        <label className="modal-display-value">{bill.shippingDistance}</label>

                                        <label className="modal-title">PT Thanh Toán: </label>
                                        <label className="modal-display-value">{bill.payWith}</label>

                                        <label className="modal-title">Khuyến Mãi: </label>
                                        <label className="modal-display-value">{bill.promotion_type}</label>
                                    </div>
                                    
                                    <div className="card-address">
                                        <label className="modal-shop-address"><strong>Đc Shop:</strong> {bill.end_address}</label>
                                        <label className="modal-customer-address"><strong>Đc Khách Hàng:</strong> {bill.start_address}</label>
                                    </div>

                                </div>)}
                                
                    </Modal.Body>

                    <Modal.Footer className="modal-footer">
                        <Button className="btn-accept" variant="primary" onClick={() => {
                                acceptTrip({id:driverListId})
                        }}>
                            Nhận Cuốc
                            </Button>
                        <Button className="btn-cancel" variant="primary" onClick={() => {
                            rejectTrip({id:driverListId })
                            handleClose()
                        }}>
                            Từ Chối Cuốc
                            </Button>
                    </Modal.Footer>
                </Modal>
            </div>
            </React.Fragment>
    );
}

const mapStateToProps = (state) => ({
    show: state.order.openModal,
    billId: state.order.billId,
    bill: state.order.bill,
    driverListId: state.order.driverListId,

})

const mapDispatchToProps = dispatch => ({
    handleShow: () => dispatch(openOrderForm),
    handleClose: () => dispatch(closeOrderForm),
    getBillInfo: (id) => dispatch(getBill(id)),
    acceptTrip: params => dispatch(acceptOrder(params)),
    rejectTrip: params => dispatch(rejectOrder(params))

}
)


export default connect(mapStateToProps,mapDispatchToProps)(Detail)