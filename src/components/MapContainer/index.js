import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

const mapStyles = {
  width: '100%',
  height: '20rem',
  position: 'relative',
};

export class MapContainer extends Component {
  render() {
    return (
      <div style={{ position: 'relative', height: '20rem' }}>
        <Map google={this.props.google} zoom={16} style={mapStyles}
          center={this.props.location}>
          <Marker
            position={this.props.location} />
        </Map>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAAepLhLqa0GZhLldm0z-Kj2JW8tTxKDRA'
})(MapContainer);