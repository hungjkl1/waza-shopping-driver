import React, { Component } from 'react';
import { Label } from 'reactstrap';
import { Card } from 'react-bootstrap';
import './style.scss';

class OrderDetail extends Component {
    render() {
        return (
            <div>
                <Card className="order-history-list-card" >
                    <div>
                        <Label className="order-history-shop-name">{this.props.shop}</Label>
                        <Label className="order-history-total-title">Tiền mặt: </Label>
                        <Label className="order-history-total-price">{this.props.price}</Label>
                    </div>
                    <div>
                        <Label className="order-history-shop-address">{this.props.shopAddress}</Label>
                    </div>
                    <div>
                        <Label className="order-history-customer-address">{this.props.customerAddress}</Label>
                    </div>
                </Card>
            </div>
        );
    }
}

export default OrderDetail;