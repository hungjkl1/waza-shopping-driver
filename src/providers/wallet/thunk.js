import axios from 'axios';
import { setWallet } from './actions';
import { transferSuccess } from '../../alert';

export const handleGetDriverWallet = () => {
  return async (dispatch) => {
    const currentDriver = JSON.parse(localStorage.getItem('driver'));
    const wallet = await axios(`https://waza-payment.herokuapp.com/wallet/${currentDriver._id}`);
    const cash = await axios(`https://waza-payment.herokuapp.com/cash/${currentDriver._id}`);
    const driverWallet = {
      wallet: wallet.data.value,
      cash: cash.data.value
    };
    dispatch(setWallet(driverWallet));
  }
};

export const handleAddMoneyToCash = (number) => {
  return async (dispatch) => {
    const currentDriver = JSON.parse(localStorage.getItem('driver'));
    const result = await axios.post('https://waza-payment.herokuapp.com/cash', { user_id: currentDriver._id, value: Number(number) });
    if (result.data.response === 'SUCCESS') {
      transferSuccess('money', 'cash', number);
      dispatch(handleGetDriverWallet());
    }
  }
};

export const handleTransferCashToWallet = (number) => {
  return async (dispatch) => {
    const currentDriver = JSON.parse(localStorage.getItem('driver'));
    const cash = await axios(`https://waza-payment.herokuapp.com/cash/${currentDriver._id}`);

    if (cash.data.value >= number) {
      const result = await axios.post('https://waza-payment.herokuapp.com/transfer_from_cash', { user_id: currentDriver._id, value: Number(number) });
      if (result.data.response === 'SUCCESS') {
        transferSuccess('cash', 'wallet', number);
        dispatch(handleGetDriverWallet());
      }
    } else {
      console.log("Your cash is not enough");
    }

  }
}

export const handleTransferWalletToCash = (number) => {
  return async (dispatch) => {
    const currentDriver = JSON.parse(localStorage.getItem('driver'));
    const wallet = await axios(`https://waza-payment.herokuapp.com/cash/${currentDriver._id}`);

    if (wallet.data.value >= number) {
      const result = await axios.post('https://waza-payment.herokuapp.com/transfer_from_wallet', { user_id: currentDriver._id, value: Number(number) })
      if (result.data.response === 'SUCCESS') {
        transferSuccess('wallet', 'cash', number);
        dispatch(handleGetDriverWallet());
      }
    } else {
      console.log("Your wallet is not enough");
    }
  }
}