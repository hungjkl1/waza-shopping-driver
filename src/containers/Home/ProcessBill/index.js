import React, { Component } from 'react'
import API from '../../../core/service'
import {connect} from 'react-redux'
import _ from 'lodash'
import { getBill, inprocessBill, doneBill, cancelBill } from '../Modal/thunk'
class InprocessBill extends Component {
    constructor(props) {
        super(props)
        this.state={
            bill:{},
            billDetail:[]
        }
        this.service = new API()
    }
    componentDidMount() {
        const billId = JSON.parse(localStorage.getItem('inprocessBill'))
        if(billId) {
            this.props.getBill(billId)
            this.service.post('getBill',{id:billId}).then(result=>{
                this.setState({
                    billDetail: result.data.results
                })
            })
        }
    }
    componentWillReceiveProps(nextProps){
        console.log(nextProps.bill)
        if(nextProps.bill&&_.isEmpty(this.state.billDetail)) {
            const billId = JSON.parse(localStorage.getItem('inprocessBill'))
            if(billId) {
                this.props.getBill(billId)
                this.service.post('getBill',{id:billId}).then(result=>{
                    this.setState({
                        billDetail: result.data.results
                    })
                })
            }
        }
    }
    render() {
        return (
            <div className='container currentBill'>
            <h5>
            Current Bill
            </h5>
            <hr></hr>
            {((!_.isEmpty(this.props.bill))&&(!_.isEmpty(this.state.billDetail))) && 
                <React.Fragment>
            <div className="modal-info">
                    <label className="modal-title">Passenger Name: </label>
                    <label className="modal-display-value">{_.get(this.props.bill,'user.fullName')}</label>
                
                    <label className="modal-title">Tổng tiền: </label>
                    <label className="modal-display-value">{this.props.bill.total}</label>

                    <label className="modal-title">Phí ship: </label>
                    <label className="modal-display-value">{this.props.bill.shippingFee}</label>

                    <label className="modal-title">Khoảng cách: </label>
                    <label className="modal-display-value">{this.props.bill.shippingDistance}</label>

                    <label className="modal-title">PT Thanh Toán: </label>
                    <label className="modal-display-value">{this.props.bill.payWith}</label>

                    <label className="modal-title">Khuyến Mãi: </label>
                    <label className="modal-display-value">{this.props.bill.promotion_type}</label>
                </div>
                <hr/>
                <div className="card-address">
                    <label className="modal-shop-address"><strong>Đc Shop:</strong> {this.props.bill.end_address}</label>
                    <label className="modal-customer-address"><strong>Đc Khách Hàng:</strong> {this.props.bill.start_address}</label>
                </div>
                <hr/>
                Bill Details:
                <hr/>
                <ul>
                {this.state.billDetail.map(item=>{
                    return <li key={item.objectId}>
                        Name: {item.product.name}
                        {' | '}
                        Quantity: {item.quantity}
                        {' | '}
                        Unit: {item.product.Unit}
                        {' | '}
                        price: {item.price}
                        
                    </li>        
                })}
                </ul>
                <hr/>
               { this.props.bill.State==='ORDER HAVE DRIVER' &&
                <button className='btn btn-primary' onClick={()=>this.props.inprocessBill(this.props.bill.objectId)}>
                    Pickup Products
                </button>
               } 
               { this.props.bill.State==='ORDER HAVE DRIVER' &&
               <button className='btn btn-danger' onClick={()=>this.props.cancelBill(this.props.bill.objectId)}>
                   Cancel Bill
               </button>
              } 
               { this.props.bill.State==='ORDER IN PROCCESS' &&
               <button className='btn btn-info' onClick={()=>this.props.doneBill(this.props.bill.objectId)}>
                   Done
               </button>
              } 
                </React.Fragment>
            }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    bill:state.order.bill
})

const mapDispatchToProps = dispatch => ({
    getBill: id => dispatch(getBill(id)),
    inprocessBill: id => dispatch(inprocessBill(id)),
    doneBill: id => dispatch(doneBill(id)),
    cancelBill: id => dispatch(cancelBill(id))
})


export default connect(mapStateToProps,mapDispatchToProps)(InprocessBill)