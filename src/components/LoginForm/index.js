import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
// Components
import InputField from '../InputField';
// Style
import './style.scss';

const LoginForm = (props) => {
  const { handleSubmit, submitting } = props
  return (
    <Form onSubmit={handleSubmit}>
      <Field name='email' type='email' placeholder='Email'
        component={InputField} />
      <Field name='password' type='password' placeholder='Password'
        component={InputField} />
      <div className='forgot-password' >
        <a
          href='/'>Forgot your password?</a>
      </div>

      <Button className='login-button'
        type="submit" disabled={submitting} block>Login</Button>
    </Form>
  )
}
export default reduxForm({
  form: 'LoginForm',
})(LoginForm)
