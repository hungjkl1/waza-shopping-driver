import React from 'react';
import { Router, navigate } from '@reach/router';
import _ from 'lodash';
// Component
import Login from '../Login';
import MainPage from '../MainPage';
import Home from "../Home";
import DeliveryHistory from '../DeliveryHistory';
import Profile from '../Profile';
import { liveQuery } from '../../helpers/liveQuery';

class App extends React.Component {
  async componentDidMount() {
    await this.props.getDriver();
    if(this.props.currentDriver) {
      liveQuery(this.props.currentDriver._id)
    }
    _.isEmpty(this.props.currentDriver) ? navigate('/') : navigate('/main');
  };
  
  render() {
    return (
      <div className="App">
        <Router>
          <Login path='/*' />
          <MainPage path='main'>
            <Home path='/' />
            <DeliveryHistory path='history' />
            <Profile path='profile' />
          </MainPage>
        </Router>
      </div>
    );
  }
}

export default App;