import React, { Component } from 'react';
import { navigate } from '@reach/router';
import _ from 'lodash';
// Components
import NavTop from '../../components/NavTop';
import NavBottom from '../../components/NavBottom';
import './style.scss';

class MainPage extends Component {
  componentDidMount() {
    _.isEmpty(this.props.currentDriver) ? navigate('/') : navigate('/main');
  };
  render() {
    return (
      <div className="main-component">
        <NavTop />
        {this.props.children}
        <NavBottom />
      </div>
    )
  }
}
export default MainPage;
