import { SET_DRIVER, REMVOE_DRIVER } from './constants';

export const setDriver = (data) => ({
  type: SET_DRIVER,
  data
})

export const removeDriver = (data) => ({
  type: REMVOE_DRIVER
})