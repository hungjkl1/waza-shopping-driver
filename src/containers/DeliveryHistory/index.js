import React, { Component } from 'react';
import { Label } from 'reactstrap';
import { Card, Button, ButtonToolbar } from 'react-bootstrap';
import './style.scss';
import OrderDetail from '../../components/OrderDetail';
import data from './data.json';


class DeliveryHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 200000,
      trips: 10
    };
  }
  render() {
    return (
      <div>
        <div className="main-history-title" >
          <Label >History</Label>
        </div>
        <div className="main-history-container">
          <Card className="history-day-card" >
            <Card.Header className="history-day-card-header" >
              <ButtonToolbar className="history-day-bar" >
                <div className="list-button-container">
                  <Button variant="light" size="sm" >Mon</Button>
                  <Button variant="light" size="sm" >Tue</Button>
                  <Button variant="light" size="sm" >Wed</Button>
                  <Button variant="light" size="sm" >Thu</Button>
                  <Button variant="light" size="sm" >Fri</Button>
                  <Button variant="light" size="sm" >Sat</Button>
                  <Button variant="light" size="sm" >Sun</Button>
                </div>
              </ButtonToolbar>
            </Card.Header>

            <Card.Body className="history-day-card-body" >
              <div className="order-history-list">
                <div className="order-history-total">
                  <Label>Tổng giá cuốc: </Label>
                  <span >{this.state.total}</span>
                </div>
                <div className="order-history-completion">
                  <Label>Hoàn thành: </Label>
                  <span >{this.state.trips}</span>
                </div>
              </div>
              {data.map((item, index) => {
                return (
                  <div className="order-history-list-container" key={index}>
                    <div className="row" >
                      <div className="col-12">
                        <OrderDetail
                          shop={item.shop}
                          price={item.price}
                          shopAddress={item.shopAddress}
                          customerAddress={item.customerAddress} />
                      </div>
                    </div>
                  </div>
                )
              })}
            </Card.Body>
          </Card>
        </div>
      </div>
    );
  }
}

export default DeliveryHistory;