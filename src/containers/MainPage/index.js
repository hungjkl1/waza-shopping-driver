import MainPage from './MainPage';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
  currentDriver: state.currentDriver
});

export default connect(mapStateToProps, null)(MainPage);