import { SET_WALLET } from './constants';

const intialState = {};

const walletReducer = (state = intialState, action) => {
  switch (action.type) {
    case SET_WALLET:
      return action.data;

    default:
      return state
  }
};
export default walletReducer;