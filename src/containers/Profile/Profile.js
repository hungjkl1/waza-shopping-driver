import React from 'react';
import { Button } from 'react-bootstrap';
import WalletForm from '../../components/WalletForm';

class Profile extends React.Component {
  handleLogout = () => {
    this.props.logout();
  }

  handleAddMoney = (values) => {
    console.log(values.money);
    this.props.addMoney(values.money);
  };

  handleCashToWallet = (values) => {
    console.log(values.cash);
    this.props.cashToWallet(values.cash);
  };

  handleWalletToCash = (values) => {
    console.log(values.wallet);
    this.props.walletToCash(values.wallet);
  };

  render() {
    const { currentDriver } = this.props;
    return (
      <div className='profile-component'>
        <div className='info-container'>
          <div className='name-and-email'>
            <div className='driver-name'>{currentDriver.fullName}</div>
            <div className='driver-email'>{currentDriver.email}</div>
          </div>

          <div className='basic-info-group'>
            <div><b>Address: </b>{currentDriver.address}</div>
            <div><b>Phone: </b>{currentDriver.phone}</div>
            <div><b>Birthday: </b>{currentDriver.birthday}</div>
          </div>

          <div className='basic-info-group'>
            <div><b>License: </b>{currentDriver.driverLicense}</div>
            <div><b>License Plate: </b>{currentDriver.licensePlate}</div>
            <div><b>ID Card: </b>{currentDriver.identityCard}</div>
          </div>

          <div className='basic-info-group'>
            <div><b>Motorcycle model: </b>{currentDriver.hangXe}</div>
          </div>
        </div>

        <hr />
        <div className="wallet-form">
          <WalletForm onSubmit={this.handleAddMoney} btnTitle={'Your add money to cash'} name={"money"} />
        </div>

        <div className="wallet-form">
          <WalletForm onSubmit={this.handleCashToWallet} btnTitle={'Your cash to wallet'} name={"cash"} />
        </div>

        <div className="wallet-form">
          <WalletForm onSubmit={this.handleWalletToCash} btnTitle={'Your wallet to cash'} name={"wallet"} />
        </div>
        <hr />

        <div className='logout-button'>
          <Button variant='danger' block onClick={this.handleLogout}>LOG OUT</Button>
        </div>

      </div>
    )
  }
}
export default Profile;