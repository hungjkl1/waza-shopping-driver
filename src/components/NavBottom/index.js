import React, { Component } from 'react'
import NavLink from '../NavLink';
import { FaHome, FaHistory, FaUserEdit } from "react-icons/fa";
import './style.scss';
export default class NavBottom extends Component {
    render() {
        return (
            <div className="navbarbottom">
                <NavLink to="/main"  ><span> <FaHome /></span> Home</NavLink>
                <NavLink to="history"><span><FaHistory /></span>History</NavLink>
                <NavLink to="profile"><span><FaUserEdit /></span>Profile</NavLink>
                {/* <button className="btn-logout "><FiLogOut /></button> */}
            </div>
        )
    }
}
