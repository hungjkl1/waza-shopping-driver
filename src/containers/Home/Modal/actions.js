export const OPEN_ORDER_FORM = 'OPEN_ORDER_FORM'
export const CLOSE_ORDER_FORM = 'CLOSE_ORDER_FORM'
export const ORDER_BILL = 'ORDER_BILL'



export const openOrderForm = (payload) => ({
    type: OPEN_ORDER_FORM,
    payload
})

export const closeOrderForm = () => ({
    type: CLOSE_ORDER_FORM,
})

export const bill = (payload) => ({
    type: ORDER_BILL,
    payload
})
