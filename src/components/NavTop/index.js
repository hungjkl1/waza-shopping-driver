import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Navbar, Nav } from 'react-bootstrap';
import { handleActiveDriver, handleInactiveDriver } from '../../providers/driver/thunk';
import './style.scss'
class NavTop extends Component {
  handleChangeStatus = () => {
    const { driver } = this.props;
    if (driver.status === 'INACTIVE') {
      this.props.active()
    } else {
      this.props.inActive()
    }
  }

  render() {
    const { driver } = this.props;
    return (
      <Navbar>
        <Navbar.Brand>
          <img src="https://waza-shopping.herokuapp.com/image/logo.png"
            width="100"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
        <Nav className='ml-auto'>
          <Button variant={driver.status === 'INACTIVE' ? 'danger' : 'success'}
            onClick={this.handleChangeStatus}>
            IS {driver.status}
          </Button>
        </Nav>
      </Navbar>
    )
  }
}

const mapStateToProps = (state) => ({
  driver: state.currentDriver
});

const mapDispatchToProps = (dispatch) => ({
  active: () => dispatch(handleActiveDriver()),
  inActive: () => dispatch(handleInactiveDriver())
});

export default connect(mapStateToProps, mapDispatchToProps)(NavTop)

