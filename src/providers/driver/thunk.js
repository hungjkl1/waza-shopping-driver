import _ from 'lodash';
import axios from 'axios';
import { setDriver, removeDriver } from './actions';
import { handleGetLocation, handleRemoveLocation } from '../location/thunk';
import { navigate } from '@reach/router';
import { loginSuccess, loginFail, isActive, isInActive } from '../../alert';

export const handleGetInfoFromLS = () => {
  return (dispatch) => {
    const currentDriver = JSON.parse(localStorage.getItem('driver'));
    console.log('CURRENT DRIVER: ', currentDriver);

    if (!_.isEmpty(currentDriver)) {
      dispatch(setDriver(currentDriver));
      dispatch(handleGetLocation(currentDriver._id));
    }
  };
};

export const handleLogin = (data) => {
  return (dispatch) => {
    axios.post('https://app-wazapassenger.herokuapp.com/api/drivers/auth', data)
      .then((result) => {
        const params = {
          headers: { "Authorization": result.data.token }
        }
        // Get drvier with token and id
        axios.get(`https://app-wazapassenger.herokuapp.com/api/drivers/${result.data.id}`, params)
          .then((driver) => {

            const driverToSave = {
              ...driver.data,
              token: result.data.token,
              status: 'INACTIVE'
            };

            dispatch(setDriver(driverToSave));
            localStorage.setItem('driver', JSON.stringify(driverToSave));
            dispatch(handleGetLocation(driver.data._id));
            navigate('/main');
            loginSuccess.fire();
          })
      })
      .catch(() => {
        loginFail.fire();
      })
  };
};

export const handleLogout = (data) => {
  return (dispatch) => {
    dispatch(handleInactiveDriver());
    dispatch(handleRemoveLocation());
    dispatch(removeDriver());
    localStorage.removeItem('driver');
    navigate('/*');
  }
};

export const handleActiveDriver = () => {
  return (dispatch) => {
    const currentDriver = JSON.parse(localStorage.getItem('driver'));
    const params = { headers: { "Authorization": currentDriver.token } };

    axios.post(`https://app-wazapassenger.herokuapp.com/api/drivers/${currentDriver._id}/alreadypickup?service:`, { serviceName: "SHOPPING" }, params)
      .then(result => {
        isActive.fire();
        const activeDriver = { ...currentDriver, status: 'ACTIVE' };
        dispatch(setDriver(activeDriver));
        localStorage.setItem('driver', JSON.stringify(activeDriver));
      })
      .catch(error => {
        console.log(error);
      })
  }
};

export const handleInactiveDriver = () => {
  return (dispatch) => {
    const currentDriver = JSON.parse(localStorage.getItem('driver'));
    const params = { headers: { "Authorization": currentDriver.token } };

    axios.post(`https://app-wazapassenger.herokuapp.com/api/drivers/${currentDriver._id}/inactivepickup?service:`, { serviceName: "SHOPPING" }, params)
      .then(result => {
        isInActive.fire();
        const inActiveDriver = { ...currentDriver, status: 'INACTIVE' };
        dispatch(setDriver(inActiveDriver));
        localStorage.setItem('driver', JSON.stringify(inActiveDriver));
      })
      .catch(error => {
        console.log(error);
      })
  }
};

