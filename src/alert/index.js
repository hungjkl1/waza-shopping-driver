import Swal from 'sweetalert2';

export const loginSuccess = Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 1500,
  icon: 'success',
  title: 'Login in successfully'
})

export const loginFail = Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 1500,
  icon: 'error',
  title: 'Wrong email or password'
})

export const isActive = Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 1500,
  icon: 'info',
  title: 'Driver is now Active'
})

export const isInActive = Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 1500,
  icon: 'info',
  title: 'Driver is Inactive'
})

export const transferSuccess = (from, to, number) => {
  Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 1500,
    icon: 'success',
    title: `Transfer ${number} VND ${from} to ${to} success`
  }).fire()
};

