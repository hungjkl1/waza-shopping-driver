import { OPEN_ORDER_FORM, CLOSE_ORDER_FORM, ORDER_BILL } from "./actions"

export const initialState = {
    bill: {},
    billId:null,
    openModal: false,
    driverListId: null
}

const Order = (state = initialState, { type, payload }) => {
    switch (type) {

    case OPEN_ORDER_FORM:
        return { ...state, openModal:true, billId:payload.id.id, driverListId:payload.driverListId }
    case CLOSE_ORDER_FORM:
        return { ...state, openModal:false }
    case ORDER_BILL:
        return {...state, bill:payload.data}
    default:
        return state
    }
}
export default Order