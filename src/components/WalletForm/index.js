import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
// Components
import InputField from '../InputField';


const WalletForm = (props) => {
  const { handleSubmit, submitting } = props
  return (
    <Form onSubmit={handleSubmit}>
      <Field name={props.name} type='number' placeholder='amount'
        component={InputField} />

      <Button
        type="submit" disabled={submitting} block>
        {props.btnTitle}
      </Button>
    </Form>
  )
}
export default reduxForm({
  form: 'WalletForm',
})(WalletForm)
