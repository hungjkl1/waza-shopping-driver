import Home from './Home';
import { connect } from 'react-redux';
import { handleGetDriverWallet } from '../../providers/wallet/thunk';

const mapStateToProps = (state) => ({
  location: state.currentLocation
});

const mapDispatchToProps = (dispatch) => ({
  getWallet: () => dispatch(handleGetDriverWallet())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
