import { setLocation } from './actions';
import socketIOClient from "socket.io-client";

const socket = socketIOClient("app-wazapassenger.herokuapp.com", {
  transports: ['polling']
});

let getLocationRealTime;
export const handleGetLocation = (userId) => {
  return (dispatch) => {
    const id = userId;
    console.log(id)
    socket.emit('join', id);
    navigator.geolocation.getCurrentPosition(info => {

      dispatch(setLocation({
        lat: info.coords.latitude,
        lng: info.coords.longitude
      }))

      socket.emit('reloadLocation', {
        lat: info.coords.latitude,
        lon: info.coords.longitude,
        userId: id
      })
    })
    getLocationRealTime = setInterval(() => {
      navigator.geolocation.getCurrentPosition(info => {

        dispatch(setLocation({
          lat: info.coords.latitude,
          lng: info.coords.longitude
        }))

        socket.emit('reloadLocation', {
          lat: info.coords.latitude,
          lon: info.coords.longitude,
          userId: id
        })
      })
    }, 1000 * 30)
  }
};

export const handleRemoveLocation = () => {
  return (dispatch) => {
    clearInterval(getLocationRealTime);
    dispatch(setLocation({}))
  }
};

