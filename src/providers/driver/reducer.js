import { SET_DRIVER, REMVOE_DRIVER } from './constants';

const intialState = {};
const driverReducer = (state = intialState, action) => {
  switch (action.type) {
    case SET_DRIVER:
      return action.data;

    case REMVOE_DRIVER:
      return {};

    default:
      return state
  }
};
export default driverReducer;