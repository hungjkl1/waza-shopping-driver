import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { navigate } from '@reach/router';
import _ from 'lodash';
// Components
import LoginForm from '../../components/LoginForm';
// Style
import './style.scss';

class Login extends React.Component {

  componentDidMount() {
    _.isEmpty(this.props.currentDriver) ? navigate('/') : navigate('/main');
  };

  handleSubmit = (values) => {

    this.props.login(values);
  }

  render() {
    return (
      <div className='login-container' >
        <Container>
          <Row>
            <Col className='login-container'>
              <h2 className='title'>Driver login</h2>
              <LoginForm onSubmit={this.handleSubmit} />
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
};
export default Login;