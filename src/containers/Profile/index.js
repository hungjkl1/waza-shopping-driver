import Profile from './Profile';
import { connect } from 'react-redux';
import { handleLogout } from '../../providers/driver/thunk';
import { handleAddMoneyToCash, handleTransferCashToWallet, handleTransferWalletToCash } from '../../providers/wallet/thunk';
import './style.scss';

const mapStateToProps = (state) => ({
  currentDriver: state.currentDriver
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(handleLogout()),
  addMoney: (number) => dispatch(handleAddMoneyToCash(number)),
  cashToWallet: (number) => dispatch(handleTransferCashToWallet(number)),
  walletToCash: (number) => dispatch(handleTransferWalletToCash(number))
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
