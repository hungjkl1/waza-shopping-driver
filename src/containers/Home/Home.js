import React, { Component } from 'react'
import GoogleMap from "../../components/MapContainer";
import './style.scss';
import ModalDriver from './Modal/Modal';
import InprocessBill from './ProcessBill';
import { connect } from 'react-redux'

class Home extends Component {
  componentDidMount() {
    this.props.getWallet();
  }
  render() {
    return (

      <div className="home-component">
        <div className="google-map-container">
          <GoogleMap location={this.props.location} />
        </div>

        <div>
          <h4 className="home-title">Your wallet infomation</h4>
          <div className="wallet-container">
            <div><b>Your cash: </b>{this.props.wallet.cash} VND</div>
            <div><b>Your wallet: </b>{this.props.wallet.wallet} VND</div>
          </div>
        </div>
        <InprocessBill/>
        {/* Modal */}
        <ModalDriver />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  wallet: state.driverWallet
});

export default connect(mapStateToProps)(Home);