import { SET_LOCATION } from './constants';

const intialState = {};

const locationReducer = (state = intialState, action) => {
  switch (action.type) {
    case SET_LOCATION:
      return action.data;

    default:
      return state
  }
};
export default locationReducer;