import core from '../../../core/service'
import { bill, closeOrderForm } from './actions'
import Axios from 'axios'
const service = new core()

export const getBill =  id => {
    return async dispatch => {
        try {
            const data =await service.post('getBillAdmin',{id})
            return dispatch(bill(data))
        } catch (err) {
    
        }
    }
}

export const acceptOrder = params => {
    return async dispatch => {
        try {
            const user = JSON.parse(localStorage.getItem('driver'))
            const acceptOrder = await service.post('acceptTrip',params)
            const {data} = acceptOrder
            data.service_trip_id = `SHOPPING4${data.service_trip_id.objectId}`
            console.log({...data,...user})
             const status = await Axios.post('https://waza-trip.herokuapp.com/driver_accepted',{
                ...data,
                ...user,
                driver_response:'Accepted',
                id_driver:user._id,
                driver_name:user.fullName
            })
            if(status.data.status) {
                const saveToBill = await service.post('updateBillDriver',{
                    id:data.service_trip_id.substring(9,data.service_trip_id.length),
                    driverId: user._id,
                    driver: user,
                })
                localStorage.setItem('inprocessBill', JSON.stringify(saveToBill.data.objectId))
                dispatch(closeOrderForm())
                dispatch(getBill(saveToBill.data.objectId))
            }
        } catch (err) {
            console.log(err)
        }
    }
}

export const rejectOrder = params => {
    return async dispatch => {
        try {
            dispatch(closeOrderForm())
            const user = JSON.parse(localStorage.getItem('driver'))
            const acceptOrder = await service.post('rejectTrip',params)
            if(acceptOrder.data.haveAcceptDriver===false) {
                const {data} = acceptOrder
                data.service_trip_id = `SHOPPING4${data.service_trip_id.objectId}`
                console.log({...data,...user})
                const toTrip = await Axios.post('https://waza-trip.herokuapp.com/driver_rejected',{
                    ...data,
                })
                console.log(toTrip)
                await service.post('updateBillState',{
                    id:data.service_trip_id.substring(9,data.service_trip_id.length),
                    State:'ORDER CANT FIND DRIVER'
                })
            }
        } catch (err) {
            console.log(err)
        }
    }
}

export const inprocessBill =  billId => {
    return async dispatch => {
    try {
        const status = await Axios.post('https://waza-trip.herokuapp.com/passenger_located',{
            service_trip_id: `SHOPPING4${billId}`
        })
        console.log(status)
        if(status.data.status) {
            await service.post('updateBillState',{
                id:billId,
                State:'ORDER IN PROCCESS'
            })
            dispatch(getBill(billId))
        } 
    } catch (err) {
        console.log(err)
    }
}
}

export const doneBill =  billId => {
    return async dispatch => {
    try {
        const status = await Axios.post('https://waza-trip.herokuapp.com/destination_located',{
            service_trip_id: `SHOPPING4${billId}`
        })
        await service.post('updateBillState',{
            id:billId,
            State:'ORDER CONFIRM FINISH'
        })
        localStorage.removeItem('inprocessBill')
        dispatch(bill({}))
        if(status.data.status) {
        }
    } catch (err) {
        console.log(err)
    }
}
}
export const cancelBill =  billId => {
    return async dispatch => {
    try {
        const user = JSON.parse(localStorage.getItem('driver'))
        const status = await Axios.post('https://waza-trip.herokuapp.com/driver_canceled',{
            service_trip_id: `SHOPPING4${billId}`,
            driver_response: "Canceled",
            id_driver: user._id
        })
        await service.post('updateBillState',{
            id:billId,
            State:'ORDER CANCEL BY DRIVER'
        })
        localStorage.removeItem('inprocessBill')
        dispatch(bill({}))
        if(status.data.status) {
        }
    } catch (err) {
        console.log(err)
    }
}
}