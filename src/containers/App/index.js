import App from './App';
import { connect } from 'react-redux';
import { handleGetInfoFromLS } from '../../providers/driver/thunk';

const mapStateToProps = (state) => ({
  currentDriver: state.currentDriver
});

const mapDispatchToProps = (dispatch) => ({
  getDriver: () => dispatch(handleGetInfoFromLS()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);